const { describe, expect, it } = require("@jest/globals");

const { filterCostumersByCountry,
        formatCustomerForStripe,
        getListCostumersFormattedForStripe,
        createCustomer,
        createMultipleCostumers,
        formatCustomerForDB,
        getListOfFormattedCostumersForDB,
      } = require('./utils');

describe('Utils tests', () => {
  it('should return a filtered list of customers by country', function () {
    const customersList = [{country:"United States"}, {country:"Spain"}, {country:"France"},
      {country:"Spain"}, {country:"Guinea"}];
    const expectedCustomersList = [{country:"Spain"}, {country:"Spain"}];
    expect(filterCostumersByCountry('Spain', customersList)).toStrictEqual(expectedCustomersList);
  });

  it('should return a formatted costumer information for Stripe account', function () {
    const customer = {
      first_name: 'Laverne',
      last_name: 'Aggio',
      email: 'laggiok@hhs.gov',
      country: 'Spain',
      address_line_1: '2 Doe Crossing Junction'
    };
    const formattedCustomer = {
      name: 'Laverne Aggio',
      email: 'laggiok@hhs.gov',
      address: {
        line1: '2 Doe Crossing Junction',
        country: 'Spain'
      }
    };
    expect(formatCustomerForStripe(customer)).toStrictEqual(formattedCustomer);
  });

  it('should return a formatted list of costumers', function () {
    const customersList = [{
      first_name: 'Laverne',
      last_name: 'Aggio',
      email: 'laggiok@hhs.gov',
      country: 'Spain',
      address_line_1: '2 Doe Crossing Junction'
    },
    {
      first_name: 'Emlynn',
      last_name: 'Avon',
      email: 'eavonb@delicious.com',
      country: 'Spain',
      address_line_1: '9805 Carioca Alley'
    }];

    const formattedCustomersList = [{
      name: 'Laverne Aggio',
      email: 'laggiok@hhs.gov',
      address: {
        line1: '2 Doe Crossing Junction',
        country: 'Spain'
        }
      },
      {
      name: 'Emlynn Avon',
      email: 'eavonb@delicious.com',
      address: {
        line1: '9805 Carioca Alley',
        country: 'Spain'
      }
    }];

    expect(getListCostumersFormattedForStripe(customersList)).toStrictEqual(formattedCustomersList);
  });

  it('should create a costumer in stripe and receive the costumer information', function () {
    const customer = {
      name: 'Laverne Aggio',
      email: 'laggiok@hhs.gov',
      address: {
        line1: '2 Doe Crossing Junction',
        country: 'Spain'
      }
    };

    let result = createCustomer(customer);
    return result
      .then(rslt => expect(rslt).toHaveProperty("name", 'Laverne Aggio'));
  });

  it('should return the a list of stripe customers', function () {
    const customersList = [{
      name: 'Laverne Aggio',
      email: 'laggiok@hhs.gov',
      address: {
        line1: '2 Doe Crossing Junction',
        country: 'Spain'
        }
      },
      {
        name: 'Emlynn Avon',
        email: 'eavonb@delicious.com',
        address: {
          line1: '9805 Carioca Alley',
          country: 'Spain'
          }
    }];

    let result = createMultipleCostumers(customersList);
    return result
      .then(rslt => expect(rslt).toEqual(
        expect.arrayContaining([
          expect.objectContaining({name: 'Laverne Aggio'}),
          expect.objectContaining({name: 'Emlynn Avon'})
        ])
      ));
  });

  it('should return a formatted costumer information for DB', function () {
    const customer = {
      id: 'cus_IE9s1Y0yKXTGeV',
      address: {
        city: null,
        country: 'Spain',
        line1: '2 Doe Crossing Junction',
        line2: null,
        postal_code: null,
        state: null
      },
      name: 'Laverne Aggio',
      email: 'laggiok@hhs.gov',
    };

    const formattedCustomer = {
      email: 'laggiok@hhs.gov',
      customerId: 'cus_IE9s1Y0yKXTGeV',
      country: 'ES',
    };

    expect(formatCustomerForDB(customer)).toStrictEqual(formattedCustomer);
  });

  it('should return a formatted list of costumers for the DB', function () {
    const customersList = [{
      id: 'cus_IE9s1Y0yKXTGeV',
      address: {
        country: 'Spain',
        line1: '2 Doe Crossing Junction'
        },
      name: 'Laverne Aggio',
      email: 'laggiok@hhs.gov',
      },
      {
        id: 'xxxxxx',
        address: {
          country: 'Spain',
          line1: '9805 Carioca Alley',
        },
      name: 'Emlynn Avon',
      email: 'eavonb@delicious.com',
    }];

    const formattedCustomersList = [{
      email: 'laggiok@hhs.gov',
      customerId: 'cus_IE9s1Y0yKXTGeV',
      country: 'ES',
      },
      {
      email: 'eavonb@delicious.com',
      customerId: 'xxxxxx',
      country: 'ES',
    }];

    expect(getListOfFormattedCostumersForDB(customersList)).toStrictEqual(formattedCustomersList);
  });
});

describe('Read & Write Json with fs', () => {
  const TestData = new Buffer('This is sample Test Data');

  it('should check if the file has been read', () => {
    const fs = {
      readFileSync: () => TestData
    };
    const ReadData = fs.readFileSync('customers.json');

    expect(ReadData).toBe(TestData);
  });

  it('should check if the file has been written', () => {
    const customers = [{
      email: 'laggiok@hhs.gov',
      customerId: 'cus_IE9s1Y0yKXTGeV',
      country: 'ES',
      },
      {
      email: 'eavonb@delicious.com',
      customerId: 'xxxxxx',
      country: 'ES',
    }];

    const fs = {
      writeFileSync: () => TestData
    };

    const writeData = fs.writeFileSync('final-customers.json', JSON.stringify(customers));

    expect(writeData).toBe(TestData);
  });
});
