const fs = require('fs');
const Stripe = require('stripe');

const STRIPE_TEST_SECRET_KEY = 'rk_test_51EDa6GIrkMUcWfFwXon9vsJYpTqX2eTqbINAUf4fZC7ivToWv59cAPoHdYhmszwL9ZKWJtUUbaCcHtpjl6rWlXLP00C1dcAoUR'
const stripe = Stripe(STRIPE_TEST_SECRET_KEY);

const getCustomersFromJson = (pathAndFileName) => {
  return JSON.parse(fs.readFileSync(pathAndFileName));
}

const filterCostumersByCountry = (country, customers) => {
  return customers.filter(customer => customer.country === country)
}

const formatCustomerForStripe = (customer) => {
  const {first_name , last_name, email, country, address_line_1,} = customer;
  return {
    name: first_name + ' ' + last_name,
    email,
    address: {
      line1: address_line_1,
      country,
    }
  }
}

const getListCostumersFormattedForStripe = (costumers) => {
  return costumers.map(customer => formatCustomerForStripe(customer));
}

const createCustomer = async (customer) => {
  return await stripe.customers.create(customer).then(data=> data).catch(()=>console.log("Customer couldn't been created"));
};

const createMultipleCostumers = async (costumers) =>  {
  const costumersList = await Promise.all(costumers.map(  customer => {
    return createCustomer(customer)
  }));
  return costumersList;
}

const getCountryIsoName = (country) => {
  let countries = JSON.parse( fs.readFileSync('countries-ISO3166.json'));
  return Object.keys(countries).find(key => countries[key] === country)
}

const formatCustomerForDB = (customer) => {
  const { email, id, address: {country},} = customer;
  const countryIsoName = getCountryIsoName(country)
  return {
    email,
    customerId: id,
    country: countryIsoName,
  }
}

const getListOfFormattedCostumersForDB = (costumers) => {
  return costumers.map(customer => formatCustomerForDB(customer));
}

const saveCostumersIntoJson = (path, customers) => {
  return fs.writeFileSync(path, JSON.stringify(customers));
}

module.exports = {
  filterCostumersByCountry,
  formatCustomerForStripe,
  getListCostumersFormattedForStripe,
  createCustomer,
  formatCustomerForDB,
  createMultipleCostumers,
  getListOfFormattedCostumersForDB,
  getCustomersFromJson,
  saveCostumersIntoJson
};