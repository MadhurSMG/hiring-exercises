const { filterCostumersByCountry,
  getListCostumersFormattedForStripe,
  createMultipleCostumers,
  getListOfFormattedCostumersForDB,
  getCustomersFromJson,
  saveCostumersIntoJson } = require('./utils');

const handler = async (country) => {

  try{
    let finalCustomers = []
    /* add code below this line */
    // get costumers from Json
    const customers = await getCustomersFromJson( 'customers.json');

    // filter the customers by country
    const costumersByCountry = await filterCostumersByCountry(country, customers);

    // transform customers to save into Stripe
    const listCostumersFormattedForStripe = await getListCostumersFormattedForStripe(costumersByCountry)

    // for each customer create a Stripe customer
    const costumersResponseStripe = await createMultipleCostumers(listCostumersFormattedForStripe)

    // push into finalCustomers the stripe customers with email, country and id as properties.
    finalCustomers = await getListOfFormattedCostumersForDB(costumersResponseStripe)

    // write finalCustomers array into final-customers.json using fs
    await saveCostumersIntoJson('final-customers.json', finalCustomers);

    /*
      finalCustomers array should look like:
      finalCustomers = [{
          email: test@test.com
          customerId: 1d833d-12390sa-9asd0a2-asdas,
          country: 'ES'
        },
        {
          email: test@test.com
          customerId: 1d833d-12390sa-9asd0a2-asdas,
          country: 'ES'
        }
      }]
    */

    /* add code above this line */

    console.log(finalCustomers)

  }catch(e){
    throw e
  }

}

handler("Spain");