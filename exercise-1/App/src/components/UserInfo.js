import React from 'react';
import { useQuery, gql } from '@apollo/client';
import Loading from './Loading'
import ErrorComponent from './ErrorComponent'

import UserInfoField from "./UserInfoField";
import PostsList from "./PostsList";

// Added a random user to have different users every time the page loads
const getRandomUserId = Math.ceil(Math.random() * 10)

const GET_USER = gql`
{
  user(id: ${getRandomUserId}) {
    email
    phone
    company {
      name
    }
    name
    username
    address {
      street
      suite
      city
    }
    posts (
      options:{
        paginate:{
           page: 1
           limit: 10
        }
      }){   
      data {
        title
      }
    }
  }
}
`;

const UserInfo = () => {
  const { loading, error, data } = useQuery(GET_USER);
  if (loading) return <Loading />;
  if (error) return <ErrorComponent error={error} />;

  const fieldsOptions = ["Address", "Email", "Phone", "Company"];
  return (
    <>
      <div className="w-full lg:w-3/5 rounded-lg lg:rounded-l-lg lg:rounded-r-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0">
        <div className="p-4 md:p-12 text-center lg:text-left">
          <div className="text-3xl font-bold pt-8 lg:pt-0">{data.user.username}</div>
          <div className="mx-auto lg:mx-0 w-4/5 pt-3 border-b-2 border-teal-500 opacity-25"></div>
          {fieldsOptions.map((field, key) => {
            const fieldIKey = data.user[field.toLocaleLowerCase().toString()]
            return <UserInfoField key={key} field={field} fieldInfo={fieldIKey}/>
          })}
        </div>
      </div>
      <PostsList data={data}/>
    </>
  );
}

export default UserInfo;