import React from "react";
import PropTypes from "prop-types";

function PostTitle(props) {
  return (
    <div>
      <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">Title: {props.postTitle}</p>
    </div>
  );
}

export default PostTitle;

PostTitle.propTypes = {
  postTitle: PropTypes.string.isRequired,
};