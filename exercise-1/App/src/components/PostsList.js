import React from 'react';
import PropTypes from "prop-types";

import PostTitle from "./PostTitle";


const PostsList = (props) => {
  const { username, posts } = props.data.user
  return (
    <div className="w-full lg:w-2/5 mx-6 lg:mx-0 h-screen py-12">
      <div className="w-full rounded-lg lg:rounded-r-lg lg:rounded-l-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0 px-12 h-full overflow-auto">
        <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">{username}'s Posts:</p>
        { posts.data.map((postTitle, key) => {
          return <PostTitle key={key} postTitle={postTitle.title}/>
        })}
      </div>
    </div>
  );
}

export default PostsList;

PostsList.propTypes = {
  field: PropTypes.number,
  data: PropTypes.object
};