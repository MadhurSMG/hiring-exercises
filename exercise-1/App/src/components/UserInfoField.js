import React from "react";
import PropTypes from 'prop-types';


function UserInfoField(props) {
  const { field, fieldInfo } = props
  const customerInfo = field === "Address" ?
    fieldInfo.street + " " + fieldInfo.suite + ", " + fieldInfo.city :
    field === "Company" ? fieldInfo.name :  fieldInfo;
  return (
    <div>
      <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start">{field}</p>
      <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">{customerInfo}</p>
    </div>
  )
}

export default UserInfoField;

UserInfoField.propTypes = {
  field: PropTypes.string,
  fieldInfo: PropTypes.oneOfType(
    [PropTypes.string,
      PropTypes.object]
  ),
};