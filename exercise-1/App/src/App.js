import React from 'react';
import { ApolloProvider } from '@apollo/client';
import { ApolloClient, InMemoryCache } from '@apollo/client';

import UserInfo from "./components/UserInfo";


const client = new ApolloClient({
  uri: 'https://graphqlzero.almansi.me/api',
  cache: new InMemoryCache()
});


function App() {
  return (
    <div className="max-w-4xl flex items-center h-auto lg:h-screen flex-wrap mx-auto my-32 lg:my-0">
      <ApolloProvider client={client}>
        <UserInfo className="max-w-sm w-full lg:max-w-full lg:flex"/>
      </ApolloProvider>
    </div>
  );
}

export default App;
